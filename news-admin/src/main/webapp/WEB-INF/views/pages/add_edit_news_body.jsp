<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<div class="col-md-9" id="mainCol">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><small>News Editor</small></h2>
        </div>
        <div class="panel-body">
            <c:choose>
                <c:when test="${newsTO.news.newsId == 0}">
                    <form:form action="/admin/addNews" commandName="newsTO" modelAttribute="newsTO" method="post">

                        <jsp:include page="add_edit_news_fields.jsp"/>
                        <button type="submit" class="btn btn-success btn-block">create</button>
                    </form:form>
                </c:when>

                <c:otherwise>
                    <form:form action="/admin/editNews" commandName="newsTO" modelAttribute="newsTO" method="post">

                        <jsp:include page="add_edit_news_fields.jsp"/>
                        <button type="submit" class="btn btn-success btn-block">update</button>
                    </form:form>
                </c:otherwise>
            </c:choose>
        </div>
    </div>

</div>

