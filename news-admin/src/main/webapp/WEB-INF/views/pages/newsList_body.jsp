<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="col-md-9" id="mainCol">
    <div class="panel panel-default">
        <div class="panel-body">
            <table cellspacing="10" align="left" style="margin: auto; border-spacing: 10px; border-collapse: inherit">
                <tr>
                    <td>
                        <h3>Search:</h3>
                    </td>
                    <form:form action="/admin/search/1" commandName="searchCriteria" method="get">
                        <td>
                            <select class="form-control" name="authorId">
                                <option disabled selected value>-- choose author --</option>
                                <c:forEach var="authorItem" items="${authors}">
                                    <c:if test="${authorItem.authorId == searchCriteria.authorId}">
                                        <option selected value="${authorItem.authorId}">${authorItem.authorName}</option>
                                    </c:if>
                                    <c:if test="${authorItem.authorId != searchCriteria.authorId}">
                                        <option value="${authorItem.authorId}">${authorItem.authorName}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>

                        <td>
                            <form:select id="tagsSelect" cssClass="form-control" path="tagIdList" multiple="true"
                                         items="${tags}"
                                         itemLabel="tagName" itemValue="tagId"/>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#tagsSelect').multiselect();
                                });
                            </script>
                        </td>

                        <td>
                            <button type="submit" class="btn btn-success">filter</button>
                        </td>

                    </form:form>

                    <form:form action="/admin/news/1" method="get">
                        <td>
                            <button type="submit" class="btn btn-danger">reset</button>
                        </td>
                    </form:form>

                </tr>
            </table>
        </div>
    </div>

    <h2>
        <small>RECENT POSTS</small>
    </h2>
    <ul class="list-group">
        <form:form action="/admin/deleteNews" modelAttribute="listNewsIds">
            <c:forEach var="newsTO" varStatus="status" items="${newsTOList}">
                <li class="list-group-item">
                    <h2><a href="<c:url value="/admin/viewNewsPage/${newsTO.news.newsId}"/>">${newsTO.news.title}</a>
                    </h2>
                    <h5>
                        <span class="glyphicon glyphicon-time"></span> Post by
                            <span
                                    class="text-primary">${newsTO.author.authorName}
                            </span>,
                        <span class="text-success">
                            <fmt:formatDate pattern="yyyy-MM-dd" value="${newsTO.news.creationDate}"/>
                        </span>
                    </h5>
                    <h6>
                        <span class="label label-info">Comments</span>
                        <span class="badge">${fn:length(newsTO.comments)}</span>
                    </h6>

                    <h5>
                        <c:forEach var="tag" items="${newsTO.tags}">
                            <span class="label label-primary">${tag.tagName}&nbsp;</span>
                        </c:forEach>
                    </h5>

                    <p>${newsTO.news.shortText}</p>

                    <div class="text-right">
                        <a href="<c:url value="/admin/openEditNews/${newsTO.news.newsId}"/>">Edit</a>
                        <form:checkbox path="idList[${status.index}]" value="${newsTO.news.newsId}"/>
                    </div>
                </li>
                <hr>
            </c:forEach>

            <br>

            <div class="text-right">
                <input class="btn btn-danger text-right" type="submit" value="delete selected news">
            </div>
        </form:form>
    </ul>

    <ul class="pagination">
        <c:choose>
            <c:when test="${currentPage - 7 > 0}">
                <c:set var="beginVal" value="${currentPage - 7}"/>
                <spring:url value="${requestScope['javax.servlet.forward.request_uri']}" var="currUrl"/>
                <spring:url value="/admin/news/" var="newsTemplate"/>
                <c:choose>
                    <c:when test="${fn:startsWith(currUrl, newsTemplate)}">
                        <li>
                            <a href="<c:url value="/admin/news/1"/>">1</a
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="<c:url value="/admin/search/1"/>">1</a>
                        </li>
                    </c:otherwise>
                </c:choose>
                <li class="disabled">
                    <a href="#">...</a>
                </li>
            </c:when>
            <c:otherwise>
                <c:set var="beginVal" value="${1}"/>
            </c:otherwise>
        </c:choose>
        <c:forEach begin="${beginVal}" end="${currentPage + 7}" var="i">
            <c:if test="${i > 0 && i <= countPages}">
                <c:choose>
                    <c:when test="${i eq currentPage}">
                        <li class="active">
                            <a>${i}</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <spring:url value="${requestScope['javax.servlet.forward.request_uri']}" var="currUrl"/>
                        <spring:url value="/admin/news/" var="newsTemplate"/>
                        <c:choose>
                            <c:when test="${fn:startsWith(currUrl, newsTemplate)}">
                                <li>
                                    <a href="<c:url value="/admin/news/${i}"/>">${i}</a
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="<c:url value="/admin/search/${i}"/>">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </c:otherwise>
                </c:choose>
            </c:if>
        </c:forEach>

        <c:if test="${currentPage + 7 < countPages}">

            <li class="disabled">
                <a href="#">...</a>
            </li>

            <spring:url value="${requestScope['javax.servlet.forward.request_uri']}" var="currUrl"/>
            <spring:url value="/admin/news/" var="newsTemplate"/>
            <c:choose>
                <c:when test="${fn:startsWith(currUrl, newsTemplate)}">
                    <li>
                        <a href="<c:url value="/admin/news/${countPages}"/>">${countPages}</a
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="<c:url value="/admin/search/${countPages}"/>">${countPages}</a>
                    </li>
                </c:otherwise>
            </c:choose>

        </c:if>
    </ul>

</div>



