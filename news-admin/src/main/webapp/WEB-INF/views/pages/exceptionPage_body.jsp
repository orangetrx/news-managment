<div class="col-md-9" id="mainCol">
    <div class="jumbotron">
        <h1>Oops.. Something going wrong :(</h1>
        <p>${exMess}</p>
    </div>

    <FORM>
        <INPUT class="btn btn-primary btn-block" Type="button" VALUE="BACK" onClick="history.go(-1);return true;">
    </FORM>
</div>