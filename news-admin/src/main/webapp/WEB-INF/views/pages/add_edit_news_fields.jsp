<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<form:hidden path="news.newsId" value="${newsTO.news.newsId}"/>
<form:hidden path="news.version" value="${newsTO.news.version}" />

    <div class="form-group">

        <c:if test="${not empty message}">
            <p class="text-danger">${message}</p>
        </c:if>

        <label form="title">Title</label>

        <form:input id="title" cssClass="form-control" size="20" maxlength="60" path="news.title"/>

        <form:errors path="news.title" cssClass="text-danger"/>
        <br>
        <label for="short">Short text</label>

        <form:textarea cssClass="form-control" id="short" path="news.shortText" rows="3" cols="35"/>

        <form:errors path="news.shortText" cssClass="text-danger"/>
        <br>
        <label for="full">Full text</label>

        <form:textarea id="full" cssClass="form-control" path="news.fullText" rows="7" cols="35"/>

        <form:errors path="news.fullText" cssClass="text-danger"/>

        <br>

        <div class="form-inline">
            <label for="authorField">Author</label>
            <select required id="authorField"  name="author" class="form-control">
                <c:forEach var="authorItem" items="${authors}">
                    <c:if test="${newsTO.author.authorId == authorItem.authorId}">
                        <option selected value="${authorItem.authorId}">${authorItem.authorName}</option>
                    </c:if>
                    <c:if test="${newsTO.author.authorId != authorItem.authorId}">
                        <option value="${authorItem.authorId}">${authorItem.authorName}</option>
                    </c:if>
                </c:forEach>
            </select>

            <label for="tagsSelect">Tags</label>
            <select id="tagsSelect" class="form-control" name="tags" multiple="multiple">
                <c:forEach var="tagItem" items="${allTags}">
                    <c:set var="isContain" value="${false}"/>

                    <c:forEach var="newsTag" items="${newsTO.tags}">
                        <c:if test="${newsTag.tagId == tagItem.tagId}">
                            <c:set var="isContain" value="${true}"/>
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${isContain}">
                            <option selected value="${tagItem.tagId}">${tagItem.tagName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${tagItem.tagId}">${tagItem.tagName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#tagsSelect').multiselect();
                });
            </script>
        </div>

    </div>


<%--
<form:input path="news.creationDate" value="${newsTO.news.creationDate}"/>--%>
