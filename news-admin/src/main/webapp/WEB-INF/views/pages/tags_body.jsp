<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-md-9" id="mainCol">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>New tag</h3>
        </div>

        <div class="panel-body">
            <form:form cssClass="form-inline" action="/admin/addTag" modelAttribute="tag" method="post">
                <label for="newTag">Tag name</label>
                <form:input id="newTag" cssClass="form-control" path="tagName"/>
                <input class="btn btn-primary" type="submit" value="save">

                <form:errors path="tagName" cssClass="text-danger"/>

            </form:form>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>
                <small>
                    Tags
                </small>
            </h2>
        </div>

        <div class="panel-body">
            <ul class="list-group">
                <c:forEach var="tagItem" items="${tags}">
                    <li class="list-group-item authorItem">
                        <form:form action="/admin/updateTag" cssClass="form-inline" modelAttribute="updatingTag"
                                   method="post">
                            <label for="Tag">Tag:</label>
                            <c:choose>
                                <c:when test="${tagItem.tagId == updatingTag.tagId}">
                                    <form:hidden path="tagId"/>
                                    <form:hidden path="version"/>
                                    <form:input cssClass="form-control" id="Tag" disabled="false" path="tagName"/>
                                    <button name="update" class="btn-primary" type="submit">update</button>
                                    <button name="delete" class="btn-danger" type="submit">delete</button>
                                    <a class="btn btn-link" href="<c:url value="/admin/tags"/>">cancel</a>
                                    <form:errors path="tagName" cssClass="text-danger"/>
                                    <c:if test="${not empty message}">
                                        <p class="text-danger">${message}</p>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <form:input cssClass="form-control" disabled="true" path="tagName"
                                                value="${tagItem.tagName}"/>
                                    <a class="btn btn-link" href="<c:url value="/admin/selectTag/${tagItem.tagId}"/>">edit</a>
                                </c:otherwise>
                            </c:choose>
                        </form:form>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>

</div>