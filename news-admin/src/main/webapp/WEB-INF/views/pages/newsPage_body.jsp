<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="col-md-9" id="mainCol">
    <ul class="pager">
        <c:if test="${not empty prev}">
            <li>
                <a href="<c:url value="/admin/viewNewsPage/${prev}"/>">prev</a>
            </li>
        </c:if>

        <c:if test="${not empty next}">
            <li>
                <a href="<c:url value="/admin/viewNewsPage/${next}"/>">next</a>
            </li>
        </c:if>
    </ul>


    <div class="jumbotron news">
        <h1>${newsTO.news.title}</h1>

        <p>${newsTO.news.fullText}</p>
    </div>
    <h5>
        <span class="glyphicon glyphicon-time"></span> Post by
                            <span
                                    class="text-primary">${newsTO.author.authorName}
                            </span>,
                        <span class="text-success"><fmt:formatDate pattern="yyyy-MM-dd"
                                                                   value="${newsTO.news.creationDate}"/>
                    </span>
    </h5>
    <h5>
        <c:forEach var="tag" items="${newsTO.tags}">
            <span class="label label-primary">${tag.tagName}</span>
        </c:forEach>
    </h5>

    <form:form action="/admin/addComment" cssClass="form-inline" modelAttribute="comment" method="post">
        <form:hidden path="newsId" value="${newsTO.news.newsId}"/>
        <form:input maxlength="100" cssClass="form-control" path="commentText"/>
        <input class="form-control" type="submit" value="Post comment">
        <form:errors path="commentText" cssClass="text-danger"/>
    </form:form>

    <ul class="list-group">
        <c:forEach var="comment" items="${newsTO.comments}">
            <li class="list-group-item">
                <a href="">
                    <fmt:formatDate type="both"
                                    dateStyle="medium" timeStyle="medium"
                                    value="${comment.creationDate}"/>
                </a>
                <br>

                <div class="row comments">
                    <span>${comment.commentText}</span>
                    <form:form cssClass="pull-right" action="/admin/deleteComment" modelAttribute="comment"
                               method="get">

                        <form:hidden path="commentId" value="${comment.commentId}"/>
                        <form:hidden path="newsId" value="${newsTO.news.newsId}"/>
                        <input class="form-control text-right" type="submit" value="&#10006;">
                    </form:form>
                </div>

            </li>

        </c:forEach>
    </ul>


</div>


