package com.epam.newsmanager.pagination;

import com.epam.newsmanager.util.PaginationData;

public final class PaginationDataHandler {
    public static final int DEFAULT_PAGE_NUMBER = 1;
    public static final int DEFAULT_RECORDS_PER_PAGE = 4;


    private PaginationDataHandler(){}

    public static PaginationData formPaginationData(int pageNumber) {
        int offset = (pageNumber - 1) * DEFAULT_RECORDS_PER_PAGE;
        final int finalOffset = offset;
        return new PaginationData(){{setOffset(finalOffset); setEndset(finalOffset + DEFAULT_RECORDS_PER_PAGE);}};
    }
}
