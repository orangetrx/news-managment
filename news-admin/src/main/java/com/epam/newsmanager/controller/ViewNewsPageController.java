package com.epam.newsmanager.controller;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.controller.constant.RequestParameterName;
import com.epam.newsmanager.controller.constant.TemplateName;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.CommentService;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
public class ViewNewsPageController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/admin/viewNewsPage/{newsId}", method = RequestMethod.GET)
    public ModelAndView loadNewsPage(@PathVariable Long newsId,
                                     @ModelAttribute(RequestParameterName.NEWS_TO) NewsTO newsTO,
                                     @ModelAttribute(RequestParameterName.COMMENT) Comment comment,
                                     ModelMap modelMap) throws ServiceException, NoSuchEntityException {
        ModelAndView modelAndView = new ModelAndView(TemplateName.NEWS_TEMPLATE);

        Long prevNewsId = null;
        Long nextNewsId = null;
        try {
            newsTO = newsManagementService.viewNewsTO(newsId);

            List<Long> newsIds = newsService.getAllNewsIds();

            prevNewsId = null;
            nextNewsId = null;
            try {
                nextNewsId = newsIds.get(newsIds.indexOf(newsId) + 1);
                prevNewsId = newsIds.get(newsIds.indexOf(newsId) - 1);
            } catch (IndexOutOfBoundsException e) {
                try {
                    prevNewsId = newsIds.get(newsIds.indexOf(newsId) - 1);
                } catch (IndexOutOfBoundsException e1) {
                }
            }
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        modelAndView.addObject(RequestParameterName.PREV, prevNewsId);
        modelAndView.addObject(RequestParameterName.NEXT, nextNewsId);


        if (comment == null) {
            comment = new Comment();
        }
        modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + RequestParameterName.ERROR,
                modelMap.get(RequestParameterName.COMMENT));
        modelAndView.addObject(RequestParameterName.NEWS_TO, newsTO);
        modelAndView.addObject(RequestParameterName.COMMENT, new Comment());
        return modelAndView;
    }

    @RequestMapping(value = "/admin/addComment", method = RequestMethod.POST)
    public String addComment(@ModelAttribute(RequestParameterName.COMMENT) @Valid Comment comment,
                             BindingResult bindingResult,
                             RedirectAttributes attributes) throws ServiceException, NoSuchEntityException {
        if (bindingResult.hasErrors()) {
            attributes.addFlashAttribute(RequestParameterName.ERROR, bindingResult);
            attributes.addFlashAttribute(RequestParameterName.COMMENT, comment);
            return "redirect:/admin/viewNewsPage/" + comment.getNewsId();
        }

        try {
            comment.setCreationDate(new Date());
            commentService.addComment(comment);
        } catch (ServiceException | NoSuchEntityException e) {
            throw e;
        }

        return "redirect:/admin/viewNewsPage/" + comment.getNewsId();
    }

    @RequestMapping(value = "/admin/deleteComment", method = RequestMethod.GET)
    public String deleteComment(@ModelAttribute(RequestParameterName.COMMENT) Comment comment) throws ServiceException {
        try {
            commentService.deleteComment(comment.getCommentId());
        } catch (ServiceException e) {
            throw e;
        }

        return "redirect:/admin/viewNewsPage/" + comment.getNewsId();
    }

}
