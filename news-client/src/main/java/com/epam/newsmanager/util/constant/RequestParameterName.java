package com.epam.newsmanager.util.constant;

public final class RequestParameterName {
    private RequestParameterName(){}

    public static final String COMMAND_NAME = "command";
    public static final String NEWS_TO = "newsTO";
    public static final String NEWS_TO_ID = "newsTOId";
    public static final String NEXT_NEWS_ID = "nextNewsId";
    public static final String PREV_NEWS_ID = "prevNewsId";
    public static final String NEWS_TO_LIST = "newsTOList";
    public static final String NEWS = "news";
    public static final String COMMENT = "comment";
    public static final String TAG = "tag";
    public static final String TAGS = "tags";
    public static final String AUTHORS = "authors";
    public static final String AUTHOR = "author";
    public static final String COMMENT_TEXT = "commentText";
    public static final String PAGE_NUMBER = "pageNumber";
    public static final String COUNT_PAGES = "countPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String SEARCH_CRITERIA = "searchCriteria";
    public static final String AUTHOR_ID = "authorId";
    public static final String TAG_ID_LIST = "tagIdList";
}
