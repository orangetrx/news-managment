package com.epam.newsmanager.controller.command.impl;

import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.service.NewsService;
import com.epam.newsmanager.util.constant.JspPageName;
import com.epam.newsmanager.util.constant.RequestParameterName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class ViewNewsCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(ViewNewsCommand.class.getPackage().getName());

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private NewsService newsService;


    @Override
    public String execute(HttpServletRequest request) {
        Long newsId = null;
        try {
            newsId = Long.parseLong(request.getParameter(RequestParameterName.NEWS_TO_ID));
            NewsTO newsTO = newsManagementService.viewNewsTO(newsId);

            List<Long> newsIds = newsService.getAllNewsIds();
            Long prevNewsId = null;
            Long nextNewsId = null;
            try {
                nextNewsId = newsIds.get(newsIds.indexOf(newsId) + 1);
                prevNewsId = newsIds.get(newsIds.indexOf(newsId) - 1);
            } catch (IndexOutOfBoundsException e) {
                try {
                    prevNewsId = newsIds.get(newsIds.indexOf(newsId) - 1);
                } catch (IndexOutOfBoundsException e1) {
                }
            }


            if (request.getParameter(RequestParameterName.PAGE_NUMBER) != null) {
                HttpSession session = request.getSession();
                session.setAttribute(RequestParameterName.PAGE_NUMBER ,Integer.parseInt(request.getParameter(RequestParameterName.PAGE_NUMBER)));
            }

            request.setAttribute(RequestParameterName.NEWS_TO, newsTO);
            request.setAttribute(RequestParameterName.PREV_NEWS_ID, prevNewsId);
            request.setAttribute(RequestParameterName.NEXT_NEWS_ID, nextNewsId);
        } catch (ServiceException | NoSuchEntityException e) {
            LOGGER.error("open single news error");
        }

        return JspPageName.NEWS_PAGE;
    }
}
