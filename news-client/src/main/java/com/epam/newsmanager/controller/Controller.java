package com.epam.newsmanager.controller;

import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.util.constant.RequestParameterName;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
@Configurable
public class Controller extends HttpServlet {

    private WebApplicationContext context;

    private static final String DEFAULT_COMMAND_NAME = "viewNewsForPageCommand";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        ServletContext servletContext = this.getServletContext();
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        String page = null;
        Command command;

        if (commandName == null) {
            commandName = DEFAULT_COMMAND_NAME;
        }

        command = (Command) context.getBean(commandName);
        if (command != null) {
            page = command.execute(request);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        String page = null;
        Command command;

        if (commandName == null) {
            commandName = DEFAULT_COMMAND_NAME;
        }

        command = (Command) context.getBean(commandName);
        if (command != null) {
            page = command.execute(request);
        }

        response.sendRedirect(page);
    }
}
