package com.epam.newsmanager.controller.command.impl;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.CommentService;
import com.epam.newsmanager.util.constant.RequestParameterName;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class AddCommentCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(AddCommentCommand.class.getPackage().getName());

    @Autowired
    private CommentService commentService;

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        if (validateParams(request)) {
            Long newsId = Long.parseLong(request.getParameter(RequestParameterName.NEWS_TO_ID));
            String commentText = request.getParameter(RequestParameterName.COMMENT_TEXT);

            try {
                commentService.addComment(
                        new Comment(){{
                            setNewsId(newsId);
                            setCommentText(commentText);
                            setCreationDate(new Date());
                        }});
            } catch (ServiceException | NoSuchEntityException e) {
                LOGGER.error("Adding comment error");
            }

            page = "/controller?command=viewNewsCommand&newsTOId=" + newsId;
        }
        else {
            page = "/controller?command=viewAllNewsCommand";
        }

        return page;
    }

    private boolean validateParams(HttpServletRequest request) {
        boolean result = true;

        try {
            Long.parseLong(request.getParameter(RequestParameterName.NEWS_TO_ID));
        } catch (NumberFormatException e) {
            result = false;
        }

        if (request.getParameter(RequestParameterName.COMMENT_TEXT).isEmpty()) {
            result = false;
        }

        int commentLength = request.getParameter(RequestParameterName.COMMENT_TEXT).length();

        if (commentLength <= 0 && commentLength > 100) {
            result = false;
        }

        return result;

    }
}
