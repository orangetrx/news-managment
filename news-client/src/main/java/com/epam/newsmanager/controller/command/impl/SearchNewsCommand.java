package com.epam.newsmanager.controller.command.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.controller.command.Command;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.AuthorService;
import com.epam.newsmanager.service.NewsManagementService;
import com.epam.newsmanager.service.NewsService;
import com.epam.newsmanager.service.TagService;
import com.epam.newsmanager.util.PaginationData;
import com.epam.newsmanager.util.constant.JspPageName;
import com.epam.newsmanager.util.constant.RequestParameterName;
import com.epam.newsmanager.util.pagination.PaginationDataHandler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class SearchNewsCommand implements Command {

    private static final Logger LOGGER = Logger.getLogger(SearchNewsCommand.class.getPackage().getName());

    @Autowired
    private NewsManagementService newsManagementService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;



    @Override
    public String execute(HttpServletRequest request) {
        SearchCriteria searchCriteria = null;
        HttpSession session = request.getSession();

        if (request.getParameter(RequestParameterName.AUTHOR_ID) != null || request.getParameterValues(RequestParameterName.TAG_ID_LIST) != null) {
            Long authorId = null;
            if (request.getParameter(RequestParameterName.AUTHOR_ID) != null) {
                authorId = Long.parseLong(request.getParameter(RequestParameterName.AUTHOR_ID));
            }

            String[] tagIdStringArr = request.getParameterValues(RequestParameterName.TAG_ID_LIST);

            List<Long> tagIdList = null;
            if (tagIdStringArr != null) {
                tagIdList = new ArrayList<>();
                for (String tagIdString : tagIdStringArr) {
                    tagIdList.add(Long.parseLong(tagIdString));
                }
            }
            searchCriteria = new SearchCriteria();
            searchCriteria.setTagIdList(tagIdList);
            searchCriteria.setAuthorId(authorId);

            session.setAttribute(RequestParameterName.SEARCH_CRITERIA, searchCriteria);
        }
        else {
            if (session != null) {
                if (session.getAttribute(RequestParameterName.SEARCH_CRITERIA) != null) {
                    searchCriteria = (SearchCriteria) session.getAttribute(RequestParameterName.SEARCH_CRITERIA);
                }
            }
        }

        int pageNumber;
        if (request.getParameter(RequestParameterName.PAGE_NUMBER) == null) {
            pageNumber = PaginationDataHandler.DEFAULT_PAGE_NUMBER;
        }
        else {
            pageNumber = Integer.parseInt(request.getParameter(RequestParameterName.PAGE_NUMBER));
        }
        PaginationData paginationData = PaginationDataHandler.formPaginationData(pageNumber);

        try {
            List<News> newsList = newsService.searchNews(searchCriteria, paginationData);
            List<NewsTO> newsTOList = new ArrayList<>();
            for (News news : newsList) {
                newsTOList.add(newsManagementService.viewNewsTO(news.getNewsId()));
            }

            Integer countPages = new Double(
                    Math.ceil(
                            newsService.getCountSearchedNews(searchCriteria)*1.0 / PaginationDataHandler.DEFAULT_RECORDS_PER_PAGE)
            ).intValue();
            List<Tag> tags = tagService.getAllTags();
            List<Author> authors = authorService.loadAllAuthors();
            request.setAttribute(RequestParameterName.COUNT_PAGES, countPages);
            request.setAttribute(RequestParameterName.PAGE_NUMBER, pageNumber);
            request.setAttribute(RequestParameterName.NEWS_TO_LIST, newsTOList);
            request.setAttribute(RequestParameterName.TAGS, tags);
            request.setAttribute(RequestParameterName.AUTHORS, authors);
            request.setAttribute(RequestParameterName.SEARCH_CRITERIA, searchCriteria);
        } catch (ServiceException | NoSuchEntityException e) {
            LOGGER.error("search criteria error");
        }

        return JspPageName.NEWS_LIST;

    }


}
