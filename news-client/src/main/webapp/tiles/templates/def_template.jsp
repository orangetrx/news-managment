<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><tiles:putAttribute name="title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap-multiselect.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-multiselect.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>">
    <style type="text/css">
        body {
            padding-top: 50px;
        }

        #masthead {
            min-height: 270px;
            background-color: #000044;
            color: #aaaacc;
        }

        #masthead h1 {
            font-size: 55px;
            line-height: 1;
        }

        #masthead .well {
            margin-top: 13%;
            background-color: #111155;
            border-color: #000033;
        }

        .icon-bar {
            background-color: #fff;
        }

        @media screen and (min-width: 768px) {
            #masthead h1 {
                font-size: 100px;
            }
        }

        .navbar-bright {
            background-color: #111155;
            color: #fff;
        }

        .navbar-bright a, #masthead a, #masthead .lead {
            color: #aaaacc;
        }

        .navbar-bright li > a:hover {
            background-color: #000033;
        }

        .affix-top, .affix {
            position: static;
        }

        @media (min-width: 979px) {
            #sidebar.affix-top {
                position: static;
                margin-top: 30px;
                width: 228px;
            }

            #sidebar.affix {
                position: fixed;
                top: 70px;
                width: 228px;
            }
        }

        #sidebar li.active {
            border: 0 #eee solid;
            border-right-width: 4px;
        }

        #mainCol h2 {
            padding-top: 55px;
            margin-top: -55px;
        }
    </style>
</head>
<body>
<tiles:insertAttribute name="header"/>
<div class="container">
    <div class="row">
        <tiles:insertAttribute name="body"/>
    </div>
</div>
<tiles:insertAttribute name="footer"/>

<script type="text/javascript">

    $(document).ready(function () {

        $('#sidebar').affix({
            offset: {
                top: 50
            }
        });

        var $body = $(document.body);
        var navHeight = $('.navbar').outerHeight(true) + 10;

        $body.scrollspy({
            target: '#leftCol',
            offset: navHeight
        });

    });

</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-40413119-1', 'bootply.com');
    ga('send', 'pageview');
</script>
</body>
</html>
