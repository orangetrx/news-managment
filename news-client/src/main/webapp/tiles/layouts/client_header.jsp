<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header class="navbar navbar-bright navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <ul class="nav navbar-bright navbar-nav">
        <li>
          <a class="navbar-brand" href="<c:url value="/controller?command=viewNewsForPageCommand&pageNumber=1"/>">News Portal</a>
        </li>
      </ul>
    </div>
  </div>
</header>