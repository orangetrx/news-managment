<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-md-9" id="mainCol">
    <div class="panel panel-default">
        <div class="panel-body">
            <table cellspacing="10" align="left" style="margin: auto; border-spacing: 10px; border-collapse: inherit">
                <tr>
                    <td>
                        <h3>Search:</h3>
                    </td>
                    <form action="controller" enctype="multipart/form-data" method="get">
                        <input type="hidden" name="command" value="searchNewsCommand">
                        <td>
                            <select required class="form-control" name="authorId">
                                <option disabled selected value>-- choose author --</option>
                                <c:forEach var="authorItem" items="${authors}">
                                    <c:if test="${authorItem.authorId == searchCriteria.authorId}">
                                        <option selected
                                                value="${authorItem.authorId}">${authorItem.authorName}</option>
                                    </c:if>
                                    <c:if test="${authorItem.authorId != searchCriteria.authorId}">
                                        <option value="${authorItem.authorId}">${authorItem.authorName}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </td>

                        <td>
                            <select id="tagsSelect" class="form-control" name="tagIdList" multiple="multiple">
                                <option disabled selected value>-- choose tags --</option>
                                <c:forEach var="tag" items="${tags}">
                                    <c:set var="matchWithSearchCriteria" value="${false}"/>
                                    <c:forEach var="tagId" items="${searchCriteria.tagIdList}">
                                        <c:if test="${tagId == tag.tagId}">
                                            <c:set var="matchWithSearchCriteria" value="${true}"/>
                                        </c:if>
                                    </c:forEach>
                                    <c:choose>
                                        <c:when test="${matchWithSearchCriteria}">
                                            <option selected value="${tag.tagId}">${tag.tagName}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${tag.tagId}">${tag.tagName}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#tagsSelect').multiselect();
                                });
                            </script>
                        </td>

                        <td>
                            <input type="submit" class="btn btn-success" value="filter">
                        </td>

                    </form>

                    <form action="controller?command=viewNewsForPageCommand&pageNumber=1" method="get">
                        <td>
                            <input type="submit" class="btn btn-danger" value="reset">
                        </td>
                    </form>
                </tr>
            </table>
        </div>
    </div>

    <h2>
        <small>RECENT POSTS</small>
    </h2>

    <ul class="list-group">


        <c:forEach var="newsTO" varStatus="status" items="${newsTOList}">
            <li class="list-group-item">
                <h2>
                    <a href="<c:url value="/controller?command=viewNewsCommand&newsTOId=${newsTO.news.newsId}&pageNumber=${pageNumber}"/>">${newsTO.news.title}</a>
                </h2>
                <h5>
                    <span class="glyphicon glyphicon-time"></span> Post by
                            <span
                                    class="text-primary">${newsTO.author.authorName}
                            </span>,
                        <span class="text-success">
                            <fmt:formatDate pattern="yyyy-MM-dd" value="${newsTO.news.creationDate}"/>
                        </span>
                </h5>
                <h6>
                    <span class="label label-info">Comments</span>
                    <span class="badge">${fn:length(newsTO.comments)}</span>
                </h6>

                <h5>
                    <c:forEach var="tag" items="${newsTO.tags}">
                        <span class="label label-primary">${tag.tagName}</span>
                    </c:forEach>
                </h5>

                <p>${newsTO.news.shortText}</p>
            </li>
            <hr>
        </c:forEach>
        <br>
    </ul>

    <ul class="pagination client">
        <c:forEach begin="1" end="${countPages}" var="i">
            <c:choose>
                <c:when test="${i eq pageNumber}">
                    <li class="active">
                        <a>${i}</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <c:set value="${param['command']}" var="currCommand"/>
                    <c:set value="viewNewsForPageCommand" var="viewAllNewsCommand"/>
                    <c:choose>
                        <c:when test="${currCommand eq viewAllNewsCommand}">
                            <li>
                                <a href="<c:url value="/controller?command=viewNewsForPageCommand&pageNumber=${i}"/>">${i}</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <a href="<c:url value="/controller?command=searchNewsCommand&pageNumber=${i}"/>">${i}</a>
                            </li>
                        </c:otherwise>
                    </c:choose>

                </c:otherwise>
            </c:choose>
        </c:forEach>
    </ul>
</div>

