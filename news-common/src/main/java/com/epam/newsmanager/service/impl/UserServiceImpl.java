package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.dao.RoleDao;
import com.epam.newsmanager.dao.UserDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * UserServiceImpl is an implementation of
 * {@link UserService} interface. At this point
 * this class are injected by {@link UserDao} and {@link RoleDao} objects via Spring. So at now it
 * have the same functionality. But also it can be expanded by another
 * functional methods. NOTE:in all implemented methods added checking for
 * passing null values;
 *
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    /**
     * Implementation of
     * {@link UserService#addUser(User)} method
     */
    @Override
    public Long addUser(User user) throws ServiceException, NoSuchEntityException {
        Long idUser;

        try {
            idUser = userDao.create(user);
            if (user.getRoles() != null) {
                for (Role role : user.getRoles()) {
                    roleDao.create(role);
                }
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (idUser == null) {
            throw new NoSuchEntityException("User was not added");
        }

        return idUser;
    }

    /**
     * Implementation of
     * {@link UserService#loadUser(long)} method
     */
    @Override
    public User loadUser(long idUser) throws ServiceException, NoSuchEntityException {
        User user;

        try {
            user = userDao.read(idUser);
            if (user != null) {
                user.setRoles(roleDao.getRolesByUserId(user.getUserId()));
            }

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (user == null) {
            throw new NoSuchEntityException(String.format("User with id: %d doesn't exist", idUser));
        }

        return user;
    }

    /**
     * Implementation of
     * {@link UserService#updateUser(User)} method
     */
    @Override
    public void updateUser(User user) throws ServiceException {
        try {
            userDao.update(user);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

}
