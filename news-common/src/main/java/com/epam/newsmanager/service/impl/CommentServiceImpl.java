package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * CommentServiceImpl is an implementation of
 * {@link CommentService} interface. At this
 * point this class are injected by
 * {@link CommentDao} object via Spring. So at now
 * it have the same functionality. But also it can be expanded by another
 * functional methods.
 *
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements CommentService {

    @Resource(name = "${daotype}CommentDao")
    private CommentDao commentDao;

    /**
     * Implementation of
     * {@link CommentService#addComment(Comment)} method
     * This method has the same functionally as
     * {@link CommentDao#create(Object)}
     */
    @Override
    public Long addComment(Comment comment) throws ServiceException, NoSuchEntityException {
        Long commentId;

        try {
            commentId = commentDao.create(comment);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (commentId == null) {
            throw new NoSuchEntityException("Comment wasn't added");
        }

        return commentId;
    }

    /**
     * Implementation of
     * {@link CommentService#deleteComment(Long)} method
     * This method has the same functionally as
     * {@link CommentDao#delete(Serializable)}
     */
    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentDao.delete(commentId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Implementation of
     * {@link CommentService#deleteCommentByNewsId(Long)} method
     * This method has the same functionally as
     * {@link CommentDao#deleteCommentsByNewsId(Long)}
     */
    @Override
    public void deleteCommentByNewsId(Long newsId) throws ServiceException {
        try {
            commentDao.deleteCommentsByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    /**
     * Implementation of
     * {@link CommentService#loadComment(Long)} method
     * This method has the same functionally as
     * {@link CommentDao#read(Serializable)}
     */
    @Override
    public Comment loadComment(Long commentId) throws ServiceException, NoSuchEntityException {
        Comment comment;

        try {
            comment = commentDao.read(commentId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (comment == null) {
            throw new NoSuchEntityException(String.format("Comment with id: %d doesn't exist", commentId));
        }

        return comment;
    }

    /**
     * Implementation of
     * {@link CommentService#loadCommentByNews(Long)} method
     * This method has the same functionally as
     * {@link CommentDao#getCommentsByNews(Long)}
     */
    @Override
    public List<Comment> loadCommentByNews(Long newsId) throws ServiceException {
        List<Comment> comments = new ArrayList<>();

        try {
            comments = commentDao.getCommentsByNews(newsId);
        } catch (DaoException e) {
        }

        return comments;
    }
}
