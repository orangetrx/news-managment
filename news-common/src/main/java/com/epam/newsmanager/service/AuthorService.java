package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;

import java.util.List;

/**
 * AuthorService is a common interface for service layer, implementation of
 * which is using for operating AUTHORS table in database;
 */
public interface AuthorService {
    /**
     * This method is using for creating new AUTHOR object in database.
     *
     * @param author
     *            AUTHOR object that contains all filled fields.
     * @return If succeed - returns ID(Long value) in database of created
     *         AUTHOR, otherwise null;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Long addAuthor(Author author) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading the AUTHOR object(row) from database if
     * it exists.
     *
     * @param authorId
     *            ID of searching AUTHOR.
     * @return author object with a specified AUTHOR ID. If it doesn't exists,
     *         returns null.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Author loadAuthor(long authorId) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading the AUTHOR object(row) from database for news if
     * it exists.
     * @param newsId
     *          ID of news
     * @return Author object with a specified AUTHOR ID. If it doesn't exists,
     *         returns null.
     * @throws ServiceException
     */
    Author loadAuthorByNewsId(long newsId) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading all authors from table AUTHOR from
     * database;
     *
     * @return list of existing authors in database(table AUTHOR);
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<Author> loadAllAuthors() throws ServiceException;

    /**
     * This method is using for loading all non expired authors(they'r expired
     * date is bigger than current date(time) and they have no published news;
     *
     * @return list of found authors(see description)
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    List<Author> loadActualAuthors() throws ServiceException;

    void updateAuthor(Author author) throws ServiceException;

    void setAuthorExpired(Author authorExpired) throws ServiceException;
}
