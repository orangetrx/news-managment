package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Role;
import com.epam.newsmanager.dao.RoleDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * RoleServiceImpl is an implementation of
 * {@link RoleService} interface. At this
 * point this class are injected by
 * {@link RoleDao} object via Spring. So at now
 * it have the same functionality. But also it can be expanded by another
 * functional methods.
 *
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class RoleServiceImpl implements RoleService {


    @Autowired
    private RoleDao roleDao;

    /**
     * Implementation of
     * {@link RoleService#addRole(Role)} method
     * This method has the same functionally as
     * {@link RoleDao#create(Object)}
     */
    @Override
    public Long addRole(Role role) throws ServiceException, NoSuchEntityException {
        Long idRole;
        try {
            idRole = roleDao.create(role);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (idRole == null) {
            throw new NoSuchEntityException("Role was not added");
        }

        return idRole;
    }

    /**
     * Implementation of
     * {@link RoleService#loadRole(long)} method
     * This method has the same functionally as
     * {@link RoleDao#read(Serializable)}
     */
    @Override
    public Role loadRole(long roleId) throws ServiceException, NoSuchEntityException {
        Role role;

        try {
            role = roleDao.read(roleId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }

        if (role == null) {
            throw new NoSuchEntityException(String.format("Role with id: %d doesn't exist", roleId));
        }

        return role;
    }
}
