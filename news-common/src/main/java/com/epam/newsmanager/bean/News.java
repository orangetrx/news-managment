package com.epam.newsmanager.bean;

import org.eclipse.persistence.annotations.ReturnInsert;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;


/**
 * News class is representing news model for table NEWS in database. NEWS table
 * fields are
 * NWS_ID;NWS_TITLE,NWS_SHORT_TEXT;NWS_FULL_TEXT;NWS_CREATION_DATE;NWS_MODIFICATION_DATE;
 */
@Entity
@Table(name = "NEWS")
@NamedQueries({
        @NamedQuery(name = "getAllNews", query = "FROM News n"),
        @NamedQuery(name = "getCountNews", query = "SELECT COUNT(n) FROM News n"),
        @NamedQuery(name = "getNewsIds", query = "select n.newsId FROM News n")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "getSortedNews", query = "SELECT *\n" +
                "  FROM ( SELECT a.*, ROWNUM rnum\n" +
                "    FROM  (SELECT \"NEWS\".NWS_ID, \"NEWS\".NWS_TITLE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_FULL_TEXT , \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_MODIFICATION_DATE, COUNT(\"COMMENTS\".COM_ID) as comCounter\n" +
                "           FROM \"NEWS\" FULL OUTER JOIN \"COMMENTS\"\n" +
                "               ON \"COMMENTS\".COM_NEWS_ID = \"NEWS\".NWS_ID\n" +
                "           GROUP BY \"NEWS\".NWS_ID, \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_FULL_TEXT, \"NEWS\".NWS_MODIFICATION_DATE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_TITLE\n" +
                "           ORDER BY comCounter DESC, \"NEWS\".NWS_MODIFICATION_DATE DESC) a\n" +
                "    WHERE ROWNUM <= ?1 )\n" +
                "WHERE rnum > ?2", resultClass = News.class),
        @NamedNativeQuery(name = "insertNewsAuthorLink", query = "insert into \"NEWS_AUTHORS\" (NWA_NEWS_ID, NWA_AUTHOR_ID) values (?1, ?2)"),
        @NamedNativeQuery(name = "insertNewsTagLink", query = "insert into \"NEWS_TAGS\" (NWT_NEWS_ID, NWT_TAG_ID) values (?1, ?2)"),
        @NamedNativeQuery(name = "deleteNewsAuthorLink", query = "delete from \"NEWS_AUTHORS\" where NWA_NEWS_ID = ?1"),
        @NamedNativeQuery(name = "deleteNewsTagLink", query = "delete from \"NEWS_TAGS\" where NWT_NEWS_ID = ?1")
})
public class News implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "NWS_ID")
    @ReturnInsert(returnOnly = true)
    private long newsId;

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "NWS_TITLE")
    private String title;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(name = "NWS_SHORT_TEXT")
    private String shortText;

    @NotNull
    @Size(min = 3, max = 2000)
    @Column(name = "NWS_FULL_TEXT")
    private String fullText;

    @Column(name = "NWS_CREATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "NWS_MODIFICATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date modificationDate;

    @Column(name = "NWS_VERSION")
    @Version
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }
    public long getNewsId() {
        return newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (newsId != news.newsId) return false;
        if (title != null ? !title.equals(news.title) : news.title != null) return false;
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
        return !(modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }
}
