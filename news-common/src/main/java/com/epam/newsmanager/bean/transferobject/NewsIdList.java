package com.epam.newsmanager.bean.transferobject;

import java.io.Serializable;

public class NewsIdList implements Serializable {
    private static final long serialVersionUID = 1l;

    private long[] idList;

    public NewsIdList(int size) {
        idList = new long[size];
    }

    public NewsIdList() {

    }

    public long[] getIdList() {
        return idList;
    }

    public void setIdList(long[] idList) {
        this.idList = idList;
    }
}
