package com.epam.newsmanager.bean;

import org.eclipse.persistence.annotations.ReturnInsert;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Author class is representing author model for table AUTHORS in database.
 * Author table fields are AUT_ID;AUT_NAME;AUT_EXPIRED;
 */
@Entity
@Table(name = "AUTHORS")
@NamedQueries({
        @NamedQuery(name = "getActualAuthors", query = "from Author a where a.expired > CURRENT_TIMESTAMP or a.expired is null"),
        @NamedQuery(name = "getAllAuthors", query = "from Author a")
})
@NamedNativeQuery(name = "getAuthorByNewsId", query = "SELECT * FROM AUTHORS INNER JOIN NEWS_AUTHORS ON NEWS_AUTHORS.NWA_NEWS_ID = ?1 and AUT_ID = NEWS_AUTHORS.NWA_AUTHOR_ID", resultClass = Author.class)
public class Author implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "AUT_ID")
    @ReturnInsert(returnOnly = true)
    private long authorId;

    @Column(name = "AUT_NAME")
    @NotNull
    @Size(min = 3,max = 30)
    private String authorName;

    @Column(name = "AUT_EXPIRED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expired;

    @Column(name = "AUT_VERSION")
    @Version
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (authorId != author.authorId) return false;
        if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (authorId ^ (authorId >>> 32));
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }
}
