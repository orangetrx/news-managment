package com.epam.newsmanager.bean.transferobject;

import java.io.Serializable;
import java.util.List;
/**
 * SearchCriteria class is using for creating objects that consist of authorId
 * and list of id's of tags. This objects are using for search functionality in
 * DAO layer.
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long authorId;
    private List<Long> tagIdList;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }
}
