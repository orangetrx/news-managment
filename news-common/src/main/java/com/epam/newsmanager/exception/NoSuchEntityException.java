package com.epam.newsmanager.exception;

/**
 *
 * This custom Exception class trows when service layer gets null entity from dao layer
 */
public class NoSuchEntityException extends ApplicationException {
    public NoSuchEntityException(String message, Exception e) {
        super(message, e);
    }

    public NoSuchEntityException(String message) {
        super(message);
    }
}
