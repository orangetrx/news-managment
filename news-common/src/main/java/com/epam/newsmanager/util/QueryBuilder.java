package com.epam.newsmanager.util;

import com.epam.newsmanager.bean.transferobject.SearchCriteria;

/**
 * QueryBuilder class is using for creating different queries for database that
 * are using in DAO layer(methods)
 *
 */
public class QueryBuilder {

    private static final String COMMA = ",";
    private static final String RIGHT_BRACKET = ")";
    private static final String AND_PART = " AND ";
    private static final String SQL_TAG_CLAUSE = " \"TAGS\".TG_ID IN(";
    private static final String HAVING_CLAUSE = " HAVING COUNT(DISTINCT \"TAGS\".TG_ID) = ";
    private static final String GROUP_CLAUSE = " GROUP BY \"NEWS\".NWS_ID,\"NEWS\".NWS_TITLE,\"NEWS\".NWS_SHORT_TEXT,\"NEWS\".NWS_FULL_TEXT,\"NEWS\".NWS_CREATION_DATE,\"NEWS\".NWS_MODIFICATION_DATE, \"NEWS\".NWS_VERSION";
    private static final String SQL_MAIN_PART = "SELECT \"NEWS\".NWS_ID, \"NEWS\".NWS_TITLE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_FULL_TEXT, \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_MODIFICATION_DATE, \"NEWS\".NWS_VERSION  FROM \"NEWS_TAGS\""
                                                + " INNER JOIN \"NEWS\" ON \"NEWS_TAGS\".NWT_NEWS_ID = \"NEWS\".NWS_ID INNER JOIN \"TAGS\" ON \"TAGS\".TG_ID = \"NEWS_TAGS\".NWT_TAG_ID"
                                                + " INNER JOIN \"NEWS_AUTHORS\" ON \"NEWS_AUTHORS\".NWA_NEWS_ID = \"NEWS\".NWS_ID INNER JOIN \"AUTHORS\" ON \"AUTHORS\".AUT_ID = \"NEWS_AUTHORS\".NWA_AUTHOR_ID"
                                                + " WHERE";
    private static final String SQL_AUTHOR_CLAUSE = " \"AUTHORS\".AUT_ID=";

    /**
     * This method is using for creating query from SerachCriteria object, that
     * must contain searching authorId and array of tag id's.If one of the
     * constituents is missing, it will build query for searching by parameter
     * that is available. If both fields is SearchCriteria are missing it will
     * return empty String;
     *
     * @param searchCriteria
     *            SearchCriteria object that contain searching authorId and list
     *            of tag id's.
     * @return proper string query if two or one of the fields in SearchCriteria
     *         is not null, and empty - if both fields are empty.
     */
    public static String getQueryByCriteria(SearchCriteria searchCriteria) {
        StringBuilder resultQuery = new StringBuilder("");
        if (searchCriteria.getAuthorId() != null || searchCriteria.getTagIdList() != null) {
            resultQuery.append(SQL_MAIN_PART);
            if (searchCriteria.getAuthorId() != null) {
                resultQuery.append(SQL_AUTHOR_CLAUSE);
                resultQuery.append(searchCriteria.getAuthorId());
            }

            if (searchCriteria.getTagIdList() != null) {
                if (searchCriteria.getAuthorId() != null) {
                    resultQuery.append(AND_PART);
                }
                resultQuery.append(SQL_TAG_CLAUSE);

                for (Long i : searchCriteria.getTagIdList()) {
                    resultQuery.append(i).append(COMMA);

                }
                resultQuery.deleteCharAt(resultQuery.length() - 1);
                resultQuery.append(RIGHT_BRACKET);
                resultQuery.append(HAVING_CLAUSE).append(
                        searchCriteria.getTagIdList().size());
            }

            resultQuery.append(GROUP_CLAUSE);

        }

        return resultQuery.toString();

    }
}
