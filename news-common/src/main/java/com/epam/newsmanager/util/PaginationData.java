package com.epam.newsmanager.util;

public class PaginationData {
    private int offset;
    private int endset;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getEndset() {
        return endset;
    }

    public void setEndset(int endset) {
        this.endset = endset;
    }
}
