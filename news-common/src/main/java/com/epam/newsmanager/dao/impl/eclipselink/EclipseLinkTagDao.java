package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EclipseLinkTagDao implements TagDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Tag> getAllTags() throws DaoException {
        return entityManager.createNamedQuery("getAllTags").getResultList();
    }

    @Override
    public List<Tag> getTagsForNews(Long idNews) throws DaoException {
        return entityManager.createNamedQuery("getTagsForNews").setParameter(1, idNews).getResultList();
    }

    @Override
    public Tag readByName(String tagName) throws DaoException {
        return (Tag) entityManager.createNamedQuery("getTagByName").setParameter("name", tagName).getSingleResult();
    }

    @Override
    public Long create(Tag newInstance) throws DaoException {
        entityManager.persist(newInstance);
        entityManager.flush();
        return newInstance.getTagId();
    }

    @Override
    public Tag read(Long id) throws DaoException {
        return entityManager.find(Tag.class, id);
    }

    @Override
    public void update(Tag updatingInstance) throws DaoException {
        entityManager.merge(updatingInstance);
    }

    @Override
    public void delete(Long id) throws DaoException {
        Tag tag = new Tag();
        tag.setTagId(id);
        entityManager.remove(tag);
    }
}
