package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.exception.DaoException;

import java.util.List;

/**
 * AuthorDao is a common interface for interactions with table AUTHORS in
 * database.In order to interact with this table, implement this interface and
 * write you realization for particular database and type of interaction(e.g.
 * JDBC,Hibernate);
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface AuthorDao extends GenericDao<Author, Long> {

    /**
     * This method is using for loading all non expired authors(their expired
     * date is bigger than current date(time);
     *
     * @return list of found authors(see description of method)
     * @throws DaoException
     *             this exception throws when on DAO layer (e.g.) SQLException
     *             caught.
     */
    List<Author> getActualAuthors() throws DaoException;

    /**
     * This method is using for loading all authors from table AUTHORS from
     * database;
     *
     * @return list of existing authors in database(table AUTHOR);
     * @throws DaoException
     *             this exception throws when on DAO layer (e.g.) SQLException
     *             caught.
     */
    List<Author> getAllAuthors() throws DaoException;

    /**
     * This method is using for loading the Author object {@link Author} from table AUTHORS that appears author of due news
     * @param id identifier of news
     * @return author object with specified AUTHOR_ID or null if searching author doesn't exist
     * @throws DaoException
     *              this exception throws when on DAO layer (e.g.) SQLException
     *              caught.
     */
    Author getAuthorByNewsId(Long id) throws DaoException;

    void setExpired(Author author) throws DaoException;
}
