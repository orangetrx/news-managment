package com.epam.newsmanager.dao.impl.oracle;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.exception.DaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.OptimisticLockException;
import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * OracleAuthorDao class is a specific realization of
 * {@link AuthorDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
@Component("oracleAuthorDao")
@Scope("singleton")
public class OracleAuthorDao implements AuthorDao {

    private static final Logger LOGGER = Logger.getLogger(OracleAuthorDao.class.getPackage().getName());

    private static final String SQL_INSERT_AUTHOR_QUERY = "insert into \"AUTHORS\" (AUT_NAME, AUT_EXPIRED) values (?, ?)";
    private static final String SQL_SELECT_AUTHOR_BY_ID_QUERY = "select AUT_ID, AUT_NAME, AUT_EXPIRED, AUT_VERSION from \"AUTHORS\" where AUT_ID = ?";
    private static final String SQL_DELETE_AUTHOR_BY_ID = "delete from \"AUTHORS\" where AUT_ID=?";
    private static final String SQL_SELECT_ACTUAL_AUTHORS_QUERY = "select AUT_ID, AUT_NAME, AUT_EXPIRED from \"AUTHORS\" where AUT_EXPIRED > SYSTIMESTAMP or AUT_EXPIRED IS NULL ";
    private static final String SQL_SELECT_ALL_AUTHORS_QUERY = "select AUT_ID, AUT_NAME, AUT_EXPIRED from \"AUTHORS\"";
    private static final String SQL_SELECT_AUTHOR_BY_NEWS_ID = "select AUT_ID, AUT_NAME, AUT_EXPIRED FROM AUTHORS INNER JOIN NEWS_AUTHORS ON NWA_NEWS_ID = ? and AUT_ID = NEWS_AUTHORS.NWA_AUTHOR_ID";
    private static final String SQL_UPDATE_AUTHOR = "update AUTHORS SET AUT_NAME=?, AUT_VERSION = ? WHERE AUT_ID=? AND AUT_VERSION = ?";
    private static final String SQL_SET_EXPIRED_AUTHOR = "update AUTHORS SET AUT_EXPIRED=?, AUT_VERSION=? WHERE AUT_ID=? AND AUT_VERSION=?";

    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of
     * {@link AuthorDao#create(Object)} method through
     * simple JDBC.
     */
    @Override
    public Long create(Author newInstance) throws DaoException {
        Long id = null;

        try(Connection connection =  dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR_QUERY, new String[]{"AUT_ID"})) {
            preparedStatement.setString(1, newInstance.getAuthorName());
            if (newInstance.getExpired() != null) {
                preparedStatement.setTimestamp(2, new Timestamp(newInstance.getExpired().getTime()));
            }
            else {
                preparedStatement.setTimestamp(2, null);
            }

            preparedStatement.execute();
            try(ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Insert author error");
            throw new DaoException("Error insert into db", e);
        }
        return id;
    }

    /**
     * Implementation of
     * {@link AuthorDao#read(Serializable)} method
     * through simple JDBC.
     */
    @Override
    public Author read(Long authorId) throws DaoException {
        Author author = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID_QUERY)) {

            statement.setLong(1, authorId);

            try(ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    author = new Author();
                    author.setAuthorId(resultSet.getLong(1));
                    author.setAuthorName(resultSet.getString(2));

                    if (resultSet.getTimestamp(3) != null) {
                        author.setExpired(new java.util.Date(resultSet.getTimestamp(3).getTime()));
                    }

                    author.setVersion(resultSet.getInt(4));

                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select author error");
            throw new DaoException("Error select from db", e);
        }
        return author;
    }

    /**
     * Implementation of
     * {@link AuthorDao#update(Object)} method
     * through simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void update(Author updatingInstance) throws DaoException {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
            statement.setString(1, updatingInstance.getAuthorName());
            statement.setInt(2, updatingInstance.getVersion() + 1);
            statement.setLong(3, updatingInstance.getAuthorId());
            statement.setInt(4, updatingInstance.getVersion());
            if (statement.executeUpdate() < 1) {
                throw new OptimisticLockException();
            }
        } catch (SQLException e) {
            LOGGER.error("update author error");
            throw new DaoException("Error update in db", e);
        }
    }

    /**
     * Implementation of
     * {@link AuthorDao#delete(Serializable)} method
     * through simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void delete(Long deletingAuthorId) throws DaoException {

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_AUTHOR_BY_ID)) {
            statement.setLong(1, deletingAuthorId);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Delete author error");
            throw new DaoException("Error delete from db", e);
        }
    }

    /**
     * Implementation of
     * {@link AuthorDao##getActualAuthors()} method
     * through simple JDBC.
     */
    @Override
    public List<Author> getActualAuthors() throws DaoException {
        List<Author> authors = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ACTUAL_AUTHORS_QUERY)) {

            while (resultSet.next()) {
                Author author = new Author();
                author.setAuthorId(resultSet.getLong(1));
                author.setAuthorName(resultSet.getString(2));
                authors.add(author);
            }

        } catch (SQLException e) {
            LOGGER.error("Select actual authors error");
            throw new DaoException("Error select from db", e);
        }

        return authors;
    }

    /**
     * Implementation of
     * {@link AuthorDao##getAllAuthors()} method
     * through simple JDBC.
     */
    @Override
    public List<Author> getAllAuthors() throws DaoException {
        List<Author> authors = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_AUTHORS_QUERY)) {

            while (resultSet.next()) {
                Author author = new Author();
                author.setAuthorId(resultSet.getLong(1));
                author.setAuthorName(resultSet.getString(2));
                authors.add(author);
            }

        } catch (SQLException e) {
            LOGGER.error("Select all authors error");
            throw new DaoException("Error select from db", e);
        }

        return authors;
    }

    /**
     * Implementation of
     * {@link AuthorDao#getAuthorByNewsId(Long)} method
     * through simple JDBC.
     */
    @Override
    public Author getAuthorByNewsId(Long newsId) throws DaoException {
        Author author = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NEWS_ID)) {
            statement.setLong(1, newsId);
            try(ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    author = new Author();
                    author.setAuthorId(resultSet.getLong(1));
                    author.setAuthorName(resultSet.getString(2));
                    if (resultSet.getTimestamp(3) != null) {
                        author.setExpired(new Date(resultSet.getTimestamp(3).getTime()));
                    }
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select author by news error");
            throw new DaoException(e.getMessage(), e);
        }

        return author;
    }

    @Override
    public void setExpired(Author author) throws DaoException {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SET_EXPIRED_AUTHOR)) {
            statement.setTimestamp(1, new Timestamp(author.getExpired().getTime()));
            statement.setInt(2, author.getVersion() + 1);
            statement.setLong(3, author.getAuthorId());
            statement.setInt(4, author.getVersion());
            if (statement.executeUpdate() < 1) {
                throw new OptimisticLockException();
            }
        } catch (SQLException e) {
            LOGGER.error("update author expired error");
            throw new DaoException(e.getMessage(), e);
        }
    }
}
