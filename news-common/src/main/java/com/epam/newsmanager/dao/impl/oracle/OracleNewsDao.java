package com.epam.newsmanager.dao.impl.oracle;

import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.NewsDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.PaginationData;
import com.epam.newsmanager.util.QueryBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.OptimisticLockException;
import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * OracleAuthorDao class is a specific realization of
 * {@link NewsDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
@Component
@Scope("singleton")
public class OracleNewsDao implements NewsDao {

    private static final Logger LOGGER = Logger.getLogger(OracleNewsDao.class.getPackage().getName());

    private static final String SQL_SELECT_ALL_NEWS_QUERY = "select * from \"NEWS\"";
    private static final String SQL_SELECT_COUNT_NEWS_QUERY = "select count(*) from \"NEWS\"";
    private static final String SQL_INSERT_NEWS_QUERY = "insert into \"NEWS\" " +
                                                    "(NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE)" +
                                                    " values (?, ?, ?, ?, ?)";

    private static final String SQL_SELECT_ONE_NEWS_BY_ID_QUERY = "select NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE, NWS_VERSION from \"NEWS\" where NWS_ID=?";
    private static final String SQL_UPDATE_NEWS_QUERY = "update \"NEWS\" set NWS_TITLE=?, NWS_SHORT_TEXT=?, NWS_FULL_TEXT=?, " +
                                                    "NWS_MODIFICATION_DATE=?, NWS_VERSION=? where NWS_ID=? AND NWS_VERSION=?";
    private static final String SQL_SELECT_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE =
            "SELECT \"NEWS\".NWS_ID, \"NEWS\".NWS_TITLE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_FULL_TEXT , \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_MODIFICATION_DATE, COUNT(\"COMMENTS\".COM_ID) as comCounter " +
            "FROM \"NEWS\" FULL OUTER JOIN \"COMMENTS\" " +
            "ON \"COMMENTS\".COM_NEWS_ID = \"NEWS\".NWS_ID " +
            "GROUP BY \"NEWS\".NWS_ID, \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_FULL_TEXT, \"NEWS\".NWS_MODIFICATION_DATE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_TITLE " +
            "ORDER BY comCounter DESC, \"NEWS\".NWS_MODIFICATION_DATE DESC";

    private static final String SQL_SELECT_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE_FOR_PAGE =
            "SELECT *\n" +
                    "  FROM ( SELECT a.*, ROWNUM rnum\n" +
                    "    FROM  (SELECT \"NEWS\".NWS_ID, \"NEWS\".NWS_TITLE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_FULL_TEXT , \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_MODIFICATION_DATE, COUNT(\"COMMENTS\".COM_ID) as comCounter\n" +
                    "           FROM \"NEWS\" FULL OUTER JOIN \"COMMENTS\"\n" +
                    "               ON \"COMMENTS\".COM_NEWS_ID = \"NEWS\".NWS_ID\n" +
                    "           GROUP BY \"NEWS\".NWS_ID, \"NEWS\".NWS_CREATION_DATE, \"NEWS\".NWS_FULL_TEXT, \"NEWS\".NWS_MODIFICATION_DATE, \"NEWS\".NWS_SHORT_TEXT, \"NEWS\".NWS_TITLE\n" +
                    "           ORDER BY comCounter DESC, \"NEWS\".NWS_MODIFICATION_DATE DESC) a\n" +
                    "    WHERE ROWNUM <= ? )\n" +
                    "WHERE rnum > ?";

    private static final String SQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
            "SELECT *\n" +
            "  FROM ( SELECT a.*, ROWNUM rnum\n" +
            "    FROM  (";
    private static final String SQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA =
                    ") a\n "+
            "    WHERE ROWNUM <= ? )\n" +
            "WHERE rnum > ?";
    private static final String SQL_DELETE_NEWS_QUERY = "delete from \"NEWS\" where NWS_ID=?";
    private static final String SQL_INSERT_NEWS_AUTHOR_LINK = "insert into \"NEWS_AUTHORS\" (NWA_NEWS_ID, NWA_AUTHOR_ID) values (?, ?)";
    private static final String SQL_INSERT_NEWS_TAG_LINK = "insert into \"NEWS_TAGS\" (NWT_NEWS_ID, NWT_TAG_ID) values (?, ?)";
    private static final String SQL_DELETE_NEWS_AUTHOR_LINK = "delete from \"NEWS_AUTHORS\" where NWA_NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_TAG_LINK = "delete from \"NEWS_TAGS\" where NWT_NEWS_ID = ?";
    private static final String SQL_SELECT_ALL_NEWS_BY_TAG = "select NWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE  from \"NEWS\" " +
                                                        "inner join \"NEWS_TAGS\" on " +
                                                        "\"NEWS\".NWS_ID = \"NEWS_TAGS\".NWT_NEWS_ID and \"NEWS_TAGS\".NWT_TAG_ID=?";

    private static final String SQL_SELECT_NEXT = "SELECT  NWS_ID, next FROM (SELECT NWS_ID, LEAD(NWS_ID) OVER (ORDER BY NWS_ID) next FROM NEWS) where NWS_ID=8";


    private static final String SQL_SELECT_NEWS_IDS = "select NEWS.NWS_ID FROM NEWS";
    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;


    /**
     * Implementation of
     * {@link NewsDao#getAllNews()} method through
     * simple JDBC.
     */
    @Override
    public List<News> getAllNews() throws DaoException {
        List<News> newsList = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE);
            ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));
                newsList.add(news);
            }
        } catch (SQLException e) {
            LOGGER.error("Select all news sql error ");
            throw new DaoException("Error readByName db", e);
        }
        return newsList;
    }

    @Override
    public List<News> getNewsPerPage(PaginationData paginationData) throws DaoException {
        List<News> newsList = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_NEWS_SORTED_BY_NUMBER_OF_COMMENTS_AND_MODIFICATION_DATE_FOR_PAGE))
        {
            statement.setInt(1, paginationData.getEndset());
            statement.setInt(2, paginationData.getOffset());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    News news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getString(2));
                    news.setShortText(resultSet.getString(3));
                    news.setFullText(resultSet.getString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));
                    newsList.add(news);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select all news sql error ");
            throw new DaoException("Error readByName db", e);
        }
        return newsList;
    }

    /**
     * Implementation of
     * {@link NewsDao#getCountOfAllNews()} method through
     * simple JDBC.
     */
    @Override
    public int getCountOfAllNews() throws DaoException {
        int count = 0;
        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_COUNT_NEWS_QUERY)) {
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error("Select count of all news error");
            throw new DaoException("Error readByName db", e);
        }

        return count;
    }

    /**
     * Implementation of
     * {@link NewsDao#insertNewsAuthorLink(Long, Long)} method through
     * simple JDBC.
     */
    @Override
    public boolean insertNewsAuthorLink(Long idAuthor, Long idNews) throws DaoException {
        try {
            return insertLink(idNews, idAuthor, SQL_INSERT_NEWS_AUTHOR_LINK);
        } catch (SQLException e) {
            LOGGER.error("Insert news-tag link error");
            throw new DaoException("Error insert into db", e);
        }
    }

    /**
     * Implementation of
     * {@link NewsDao#insertNewsTagLink(Long, Long)} method through
     * simple JDBC.
     */
    @Override
    public boolean insertNewsTagLink(Long idNews, Long idTag) throws DaoException {
        try {
            return insertLink(idNews, idTag, SQL_INSERT_NEWS_TAG_LINK);
        } catch (SQLException e) {
            LOGGER.error("Insert news-tag link error");
            throw new DaoException("Error insert into db", e);
        }
    }

    private boolean insertLink(Long id1, Long id2, String sqlReq) throws SQLException {
        boolean result = false;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlReq)) {
            statement.setLong(1, id1);
            statement.setLong(2, id2);
            result = statement.execute();
        }
        return result;
    }

    /**
     * Implementation of
     * {@link NewsDao#deleteNewsAuthorLink(Long)} method through
     * simple JDBC.
     */
    @Override
    public boolean deleteNewsAuthorLink(Long idNews) throws DaoException {
        try {
            return deleteLink(idNews, SQL_DELETE_NEWS_AUTHOR_LINK);
        } catch (SQLException e) {
            LOGGER.error("Delete news-author link error");
            throw new DaoException("Error delete from db", e);
        }
    }


    /**
     * Implementation of
     * {@link NewsDao#deleteNewsTagLink(Long)} method through
     * simple JDBC.
     */
    @Override
    public boolean deleteNewsTagLink(Long idNews) throws DaoException {
        try {
            return deleteLink(idNews, SQL_DELETE_NEWS_TAG_LINK);
        } catch (SQLException e) {
            LOGGER.error("Delete news-tag link error");
            throw new DaoException("Error delete from db", e);
        }
    }

    private boolean deleteLink(long id, String sqlReq) throws SQLException {
        boolean result = false;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlReq)){

            statement.setLong(1, id);
            result = statement.execute();
        }

        return result;
    }


    /**
     * Implementation of
     * {@link NewsDao#getNewsByTag(Long)} method through
     * simple JDBC.
     */
    @Override
    public List<News> getNewsByTag(Long tagId) throws DaoException {
        List<News> newsList = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_NEWS_BY_TAG)) {
            statement.setLong(1, tagId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    News news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getString(2));
                    news.setShortText(resultSet.getString(3));
                    news.setFullText(resultSet.getString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));
                    newsList.add(news);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select news by id error");
            throw new DaoException("Error selcet from db", e);
        }
        return newsList;
    }

    /**
     * Implementation of
     * {@link NewsDao#getNewsBySearchCriteria(SearchCriteria, PaginationData)} method through
     * simple JDBC.
     */
    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria, PaginationData paginationData) throws DaoException {
        List<News> newsList = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_LEFT_PART_OF_QUERY_FOR_SEARCH_CRITERIA +
                    QueryBuilder.getQueryByCriteria(searchCriteria) + SQL_RIGHT_PART_OF_QUERY_FOR_SEARCH_CRITERIA))
        {
            statement.setInt(1, paginationData.getEndset());
            statement.setInt(2, paginationData.getOffset());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    News news = new News();
                    news.setNewsId(resultSet.getLong(1));
                    news.setTitle(resultSet.getString(2));
                    news.setShortText(resultSet.getString(3));
                    news.setFullText(resultSet.getString(4));
                    news.setCreationDate(resultSet.getTimestamp(5));
                    news.setModificationDate(resultSet.getDate(6));

                    newsList.add(news);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select news by criteria error");
            throw new DaoException("Error selcet from db", e);
        }

        return newsList;
    }

    @Override
    public int getCountNewsBySearchCriteria(SearchCriteria searchCriteria) throws DaoException {

        List<News> newsList = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueryBuilder.getQueryByCriteria(searchCriteria)))
        {
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getLong(1));
                news.setTitle(resultSet.getString(2));
                news.setShortText(resultSet.getString(3));
                news.setFullText(resultSet.getString(4));
                news.setCreationDate(resultSet.getTimestamp(5));
                news.setModificationDate(resultSet.getDate(6));

                newsList.add(news);
            }
        } catch (SQLException e) {
            LOGGER.error("Select news by criteria error");
            throw new DaoException("Error selcet from db", e);
        }

        return newsList.size();
    }

    @Override
    public List<Long> getAllNewsIdList() throws DaoException {
        List<Long> idList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEWS_IDS)) {

            while (resultSet.next()) {
                idList.add(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            LOGGER.error("Select news id list error");
            throw new DaoException("Error selcet from db", e);
        }

        return idList;
    }

    /**
     * Implementation of
     * {@link NewsDao#create(Object)}  method through
     * simple JDBC.
     */
    @Override
    public Long create(News newInstance) throws DaoException {
        Long id = null;
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEWS_QUERY, new String[]{"NWS_ID"})) {
            statement.setString(1, newInstance.getTitle());
            statement.setString(2, newInstance.getShortText());
            statement.setString(3, newInstance.getFullText());
            statement.setTimestamp(4, new Timestamp(newInstance.getCreationDate().getTime()));
            statement.setDate(5, new Date(newInstance.getModificationDate().getTime()));
            statement.execute();
            try(ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Create news error");
            throw new DaoException("Error insert into db", e);
        }

        return id;
    }

    /**
     * Implementation of
     * {@link NewsDao#read(Serializable)} method through
     * simple JDBC.
     */
    @Override
    public News read(Long id) throws DaoException {
        News news = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ONE_NEWS_BY_ID_QUERY)) {

            preparedStatement.setLong(1, id);

            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    news = new News();
                    news.setNewsId(id);
                    news.setTitle(resultSet.getString(2));
                    news.setShortText(resultSet.getString(3));
                    news.setFullText(resultSet.getString(4));
                    news.setCreationDate(new java.util.Date(resultSet.getTimestamp(5).getTime()));
                    news.setModificationDate(resultSet.getDate(6));
                    news.setVersion(resultSet.getInt(7));
                }
            }

        } catch (SQLException e) {
            LOGGER.error("Read news error");
            throw new DaoException("Error readByName from db", e);
        }
        return news;
    }

    /**
     * Implementation of
     * {@link NewsDao#update(Object)} method through
     * simple JDBC.
     */
    @Override
    public void update(News updatingInstance) throws DaoException {

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_NEWS_QUERY)) {

            statement.setString(1, updatingInstance.getTitle());
            statement.setString(2, updatingInstance.getShortText());
            statement.setString(3, updatingInstance.getFullText());
            statement.setDate(4, new Date(updatingInstance.getModificationDate().getTime()));
            statement.setInt(5, updatingInstance.getVersion() + 1);
            statement.setLong(6, updatingInstance.getNewsId());
            statement.setInt(7, updatingInstance.getVersion());

            if (statement.executeUpdate() < 1) {
                throw new OptimisticLockException();
            }
        } catch (SQLException e) {
            LOGGER.error("Update news error");
            throw new DaoException("Error db update", e);
        }

    }

    /**
     * Implementation of
     * {@link NewsDao#delete(Serializable)} method through
     * simple JDBC.
     */
    @Override
    public void delete(Long id) throws DaoException {

        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_NEWS_QUERY)) {

            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Delete news error");
            throw new DaoException("Error delete from db", e);
        }
    }
}
