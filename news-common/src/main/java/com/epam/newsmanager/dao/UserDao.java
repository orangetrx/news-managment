package com.epam.newsmanager.dao;

import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.exception.DaoException;

/**
 * RoleDao is a common interface for interactions with table USERS in database.
 * In order to interact with this tables,
 * implement this interface and write you realization for particular database;
 * This interface extends by generic interface that contains C.R.U.D. operations {@link GenericDao}
 */
public interface UserDao extends GenericDao<User, Long> {
    User checkUser(User user) throws DaoException;
    User readUserByLogin(String login) throws DaoException;
}
