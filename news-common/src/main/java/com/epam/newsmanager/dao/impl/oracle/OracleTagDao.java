package com.epam.newsmanager.dao.impl.oracle;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.TagDao;
import com.epam.newsmanager.exception.DaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.OptimisticLockException;
import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * OracleAuthorDao class is a specific realization of
 * {@link TagDao} interface. According to this
 * class you can interact with database through simple JDBC. Also it's using
 * injection of dataSource through Spring.
 */
@Component
@Scope("singleton")
public class OracleTagDao implements TagDao {
    private static final Logger LOGGER = Logger.getLogger(OracleTagDao.class.getPackage().getName());

    private static final String SQL_INSERT_TAG_QUERY = "insert into \"TAGS\" (TG_NAME) values (?)";
    private static final String SQL_SELECT_TAG_QUERY_BY_ID = "select TG_NAME, TG_VERSION from \"TAGS\" where TG_ID = ?";
    private static final String SQL_SELECT_TAG_QUERY_BY_NAME = "select TG_ID from \"TAGS\" where TG_NAME = ?";
    private static final String SQL_SELECT_ALL_TAGS_QUERY = "select TG_ID, TG_NAME from \"TAGS\"";
    private static final String SQL_SELECT_TAGS_BY_NEWS_QUERY = "select TG_ID, TG_NAME from \"TAGS\" " +
                                                            "inner join \"NEWS_TAGS\" on " +
                                                            "\"TAGS\".TG_ID = \"NEWS_TAGS\".NWT_TAG_ID and  \"NEWS_TAGS\".NWT_NEWS_ID=?";
    private static final String SQL_UPDATE_TAG = "update TAGS SET TG_NAME=?, TG_VERSION=? WHERE TG_ID=? AND TG_VERSION = ?";
    private static final String SQL_DELETE_TAG = "delete FROM TAGS WHERE TG_ID=?";
    /**
     * Some datasource that already configured for concrete
     *            database(e.g for oracle url/pass/login/driver).
     *            Annotation @Autowired is using for spring DI of DataSource.
     *            Configuration for oracle database you can see in
     *            database.properties file.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of
     * {@link TagDao#create(Object)} method through
     * simple JDBC.
     */
    @Override
    public Long create(Tag newInstance) throws DaoException {
        Long id = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_TAG_QUERY, new String[]{"TG_ID"})) {

            preparedStatement.setString(1, newInstance.getTagName());

            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    id = resultSet.getLong(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Insert tag error");
            throw new DaoException("Error insert into db", e);
        }

        return id;
    }

    /**
     * Implementation of
     * {@link TagDao#read(Serializable)} method through
     * simple JDBC.
     */
    @Override
    public Tag read(Long id) throws DaoException {
        Tag tag = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TAG_QUERY_BY_ID)) {
            statement.setLong(1, id);
            try(ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    tag = new Tag();
                    tag.setTagId(id);
                    tag.setTagName(resultSet.getString(1));
                    tag.setVersion(resultSet.getInt(2));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select tag error");
            throw new DaoException("Error select from db", e);
        }

        return tag;
    }

    /**
     * Implementation of
     * {@link TagDao#readByName(String)} method through
     * simple JDBC.
     */
    @Override
    public Tag readByName(String tagName) throws DaoException {
        Tag tag = null;

        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TAG_QUERY_BY_NAME)) {
            statement.setString(1, tagName);
            try(ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    tag = new Tag();
                    tag.setTagId(resultSet.getLong(1));
                    tag.setTagName(tagName);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select tag error");
            throw new DaoException("Error select from db", e);
        }

        return tag;
    }


    /**
     * Implementation of
     * {@link TagDao#update(Object)} method through
     * simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void update(Tag updatingInstance) throws DaoException {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TAG))
        {
            statement.setString(1, updatingInstance.getTagName());
            statement.setInt(2, updatingInstance.getVersion() + 1);
            statement.setLong(3, updatingInstance.getTagId());
            statement.setInt(4, updatingInstance.getVersion());
            if (statement.executeUpdate() < 1) {
                throw new OptimisticLockException();
            }
        } catch (SQLException e) {
            LOGGER.error("Update tag error");
            throw new DaoException("Error update in db", e);
        }
    }

    /**
     * Implementation of
     * {@link TagDao#delete(Serializable)} method through
     * simple JDBC.
     * In this case operation isn't available
     */
    @Override
    public void delete(Long id) throws DaoException {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE_TAG)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Implementation of
     * {@link TagDao#getAllTags()} method through
     * simple JDBC.
     */
    @Override
    public List<Tag> getAllTags() throws DaoException {
        List<Tag> tags = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_TAGS_QUERY)) {

            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setTagId(resultSet.getLong(1));
                tag.setTagName(resultSet.getString(2));
                tags.add(tag);
            }

        } catch (SQLException e) {
            LOGGER.error("Select tags error");
            throw new DaoException("Error select from db", e);
        }

        return tags;
    }

    /**
     * Implementation of
     * {@link TagDao#getTagsForNews(Long)} method through
     * simple JDBC.
     */
    @Override
    public List<Tag> getTagsForNews(Long idNews) throws DaoException {
        List<Tag> tagsForNews = new ArrayList<>();
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_TAGS_BY_NEWS_QUERY)) {

            statement.setLong(1, idNews);
            try(ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Tag tag = new Tag();
                    tag.setTagId(resultSet.getLong(1));
                    tag.setTagName(resultSet.getString(2));
                    tagsForNews.add(tag);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Select tags for news error");
            throw new DaoException("Error select from db", e);
        }
        return tagsForNews;
    }
}
