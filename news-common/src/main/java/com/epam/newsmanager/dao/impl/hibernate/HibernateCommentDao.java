package com.epam.newsmanager.dao.impl.hibernate;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.exception.DaoException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HibernateCommentDao implements CommentDao {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SQL_SELECT_COUNT_OF_COMMENTS_BY_NEWS = "select count(COM_ID) FROM \"COMMENTS\" WHERE COM_NEWS_ID = :newsId";

    private static final String SQL_SELECT_COMMENTS_FOR_NEWS = "FROM Comment comment where COM_NEWS_ID = :newsId ORDER BY COM_CREATION_DATE DESC";

    @Override
    public List<Comment> getCommentsByNews(Long newsId) throws DaoException {
        Session session = null;
        List<Comment> comments = new ArrayList<>();
        String query = SQL_SELECT_COMMENTS_FOR_NEWS;
        session = sessionFactory.getCurrentSession();
        comments = session.getNamedQuery("getCommentsForNews").setParameter("newsId", newsId).setCacheable(true).list();

        return comments;
    }

    @Override
    public boolean deleteCommentsByNewsId(final Long newsId) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        session.delete(comment);
        session.flush();

        return true;
    }

    @Override
    public int getCountOfCommentsByNewsId(Long newsId) throws DaoException {
        Session session = null;
        int count = 0;
        session = sessionFactory.getCurrentSession();
        count = ((int) session.getNamedQuery("getCountOfCommentsForNews").setParameter("newsId", newsId).setCacheable(true).uniqueResult());

        return count;
    }

    @Override
    public Long create(Comment newInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.save(newInstance);

        return newInstance.getCommentId();
    }

    @Override
    public Comment read(Long id) throws DaoException {
        Session session = null;
        Comment comment = null;
        session = sessionFactory.getCurrentSession();
        comment = session.get(Comment.class, id);

        return comment;
    }

    @Override
    public void update(Comment updatingInstance) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(updatingInstance);
        session.flush();
    }

    @Override
    public void delete(final Long id) throws DaoException {
        Session session = null;
        session = sessionFactory.getCurrentSession();
        Comment comment = new Comment();
        comment.setCommentId(id);
        session.delete(comment);
        session.flush();
    }
}
