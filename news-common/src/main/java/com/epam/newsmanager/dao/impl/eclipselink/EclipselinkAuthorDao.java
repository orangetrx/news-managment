package com.epam.newsmanager.dao.impl.eclipselink;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.exception.DaoException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EclipseLinkAuthorDao implements AuthorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Author> getActualAuthors() throws DaoException {
        return entityManager.createNamedQuery("getActualAuthors").getResultList();
    }

    @Override
    public List<Author> getAllAuthors() throws DaoException {
        return entityManager.createNamedQuery("getAllAuthors").getResultList();
    }

    @Override
    public Author getAuthorByNewsId(Long id) throws DaoException {
        return (Author) entityManager.createNamedQuery("getAuthorByNewsId").setParameter("1", id).getSingleResult();
    }

    @Override
    public void setExpired(Author author) throws DaoException {
        entityManager.merge(author);
    }

    @Override
    public Long create(Author newInstance) throws DaoException {
        entityManager.persist(newInstance);
        entityManager.flush();
        return newInstance.getAuthorId();
    }

    @Override
    public Author read(Long id) throws DaoException {
        return entityManager.find(Author.class, id);
    }

    @Transactional
    @Override
    public void update(Author updatingInstance) throws DaoException {
        entityManager.merge(updatingInstance);
    }

    @Override
    public void delete(Long id) throws DaoException {
        entityManager.remove(read(id));
    }
}
