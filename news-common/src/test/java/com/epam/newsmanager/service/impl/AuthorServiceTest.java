package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.impl.oracle.OracleAuthorDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

@Transactional(rollbackFor = {ServiceException.class})
public class AuthorServiceTest {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private OracleAuthorDao authorDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddAuthor() throws ServiceException, DaoException, NoSuchEntityException {
        Author dummyAuthor = new Author();
        long dummyAuthorId = 1;
        when(authorService.addAuthor(dummyAuthor)).thenReturn(dummyAuthorId);

        long actualId = authorService.addAuthor(dummyAuthor);

        verify(authorDao, atLeastOnce()).create(dummyAuthor);
        assertEquals(actualId, dummyAuthorId);
    }

    @Test
    public void testLoadAuthor() throws ServiceException, DaoException, NoSuchEntityException {
        long authorId = 1l;
        Author dummyAuthor = new Author();
        Author expectedAuthor;

        when(authorService.loadAuthor(authorId)).thenReturn(dummyAuthor);
        expectedAuthor = authorService.loadAuthor(authorId);

        verify(authorDao, atLeastOnce()).read(authorId);
        assertEquals(expectedAuthor, dummyAuthor);
    }

    @Test
    public void testLoadAuthorByNewsId() throws ServiceException, DaoException, NoSuchEntityException {
        long newsId = 1l;
        Author dummyAuthor = new Author();
        Author expectedAuthor;

        when(authorService.loadAuthorByNewsId(newsId)).thenReturn(dummyAuthor);
        expectedAuthor = authorService.loadAuthorByNewsId(newsId);

        verify(authorDao, atLeastOnce()).getAuthorByNewsId(newsId);
        assertEquals(expectedAuthor, dummyAuthor);
    }

    @Test
    public void testLoadAllAuthors() throws ServiceException, DaoException {
        List<Author> dummyAuthors = new ArrayList<>();
        List<Author> expectedAuthors;

        when(authorService.loadAllAuthors()).thenReturn(dummyAuthors);
        expectedAuthors = authorService.loadAllAuthors();

        verify(authorDao, atLeastOnce()).getAllAuthors();
        assertEquals(expectedAuthors, dummyAuthors);
    }

    @Test
    public void testLoadActualAuthors() throws ServiceException, DaoException {
        List<Author> dummyAuthors = new ArrayList<>();
        List<Author> expectedAuthors;

        when(authorService.loadActualAuthors()).thenReturn(dummyAuthors);
        expectedAuthors = authorService.loadActualAuthors();

        verify(authorDao, atLeastOnce()).getActualAuthors();
        assertEquals(expectedAuthors, dummyAuthors);
    }
}
