package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.dao.AuthorDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:com/epam/newsmanager/dao/AuthorDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/NewsDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/news_authors_dataset.xml"}, type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/news_authors_dataset.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoTest {

    @Autowired
    private AuthorDao authorDao;

    @Autowired
    private DataSource dataSource;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInAuthors(dataSource);
        DbTestUtil.resetAutoIncrementInNews(dataSource);
    }

    @Test
    public void testCreate() throws DaoException {
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorName("author");
        expectedAuthor.setExpired(new Date(new Date().getTime()));

        long authorId = authorDao.create(expectedAuthor);
        expectedAuthor.setAuthorId(authorId);

        Author actualAuthor = authorDao.read(authorId);

        Assert.assertEquals(actualAuthor, expectedAuthor);
    }

    @Test
    public void testRead() throws DaoException, ParseException {
        Author expectedAuthor = new Author();
        expectedAuthor.setAuthorId(1);
        expectedAuthor.setAuthorName("author1");
        expectedAuthor.setExpired(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("2017-02-17 20:20:55"));
        Author actualAuthor = authorDao.read(new Long(1));

        Assert.assertEquals(actualAuthor, expectedAuthor);

    }

    @Test
    public void testDelete() throws DaoException {
        Author actualAuthor;
        long dummyId = 1;
        authorDao.delete(dummyId);
        actualAuthor = authorDao.read(dummyId);

        Assert.assertNull(actualAuthor);
    }

    @Test
    public void testGetActualAuthors() throws DaoException {
        List<Author> actualAuthorList;
        actualAuthorList = authorDao.getActualAuthors();
        Assert.assertEquals(actualAuthorList.size(), 1);
    }

    @Test
    public void testGetAllAuthors() throws DaoException {
        List<Author> actualAuthorList;
        actualAuthorList = authorDao.getAllAuthors();
        Assert.assertEquals(actualAuthorList.size(), 3);
    }

    @Test
    public void testGetAuthorByNewsId() throws DaoException {
        long dummyNewsId = 1;

        long expectedAuthorId = 2;

        Author author = authorDao.getAuthorByNewsId(dummyNewsId);
        Assert.assertEquals(expectedAuthorId, author.getAuthorId());
    }


}
