package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.dao.CommentDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = {"classpath:com/epam/newsmanager/dao/NewsDaoTest.xml",
        "classpath:com/epam/newsmanager/dao/CommentDaoTest.xml"})
@DatabaseTearDown(value = "classpath:com/epam/newsmanager/dao/CommentDaoTest.xml", type = DatabaseOperation.DELETE)
public class CommentDaoTest {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private DataSource dataSource;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInComments(dataSource);
        DbTestUtil.resetAutoIncrementInNews(dataSource);
    }

    @Test
    public void testGetCommentsByNews() throws DaoException {
        int expectedCountNews = 2;

        long dummyNewsId = 1;

        List<Comment> actualCommentsList = commentDao.getCommentsByNews(dummyNewsId);
        Assert.assertEquals(expectedCountNews, actualCommentsList.size());
    }

    @Test
    public void testDeleteCommentsByNewsId() throws DaoException {
        int expectedCountComments = 0;

        long dummyNewsId = 2;
        commentDao.deleteCommentsByNewsId(dummyNewsId);
        int actualCountComments = commentDao.getCountOfCommentsByNewsId(dummyNewsId);

        Assert.assertEquals(expectedCountComments, actualCountComments);
    }

    @Test
    public void testGetCountOfCommentsByNewsId() throws DaoException {
        int expectedCountComments = 1;
        long dummyNewsId = 2;
        int actualCountComments = commentDao.getCountOfCommentsByNewsId(dummyNewsId);

        Assert.assertEquals(expectedCountComments, actualCountComments);
    }

    @Test
    public void testCreate() throws DaoException {
        Comment expectedComment = new Comment();
        expectedComment.setNewsId(1);
        expectedComment.setCommentText("text");
        expectedComment.setCreationDate(new Date(new Date().getTime()));

        long commentId = commentDao.create(expectedComment);
        expectedComment.setCommentId(commentId);

        Comment actualComment = commentDao.read(commentId);

        Assert.assertEquals(actualComment, expectedComment);
    }

    @Test
    public void testRead() throws ParseException, DaoException {
        Comment expectedComment = new Comment();
        expectedComment.setCommentId(1);
        expectedComment.setNewsId(1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        expectedComment.setCreationDate(simpleDateFormat.parse("2017-02-17 20:20:55"));
        expectedComment.setCommentText("text1");

        Comment actualComment = commentDao.read(new Long(1));
        Assert.assertEquals(actualComment, expectedComment);
    }

    @Test
    public void testDelete() throws DaoException {
        long dummyCommentId = 1;

        commentDao.delete(dummyCommentId);

        Comment actualComment = commentDao.read(dummyCommentId);
        Assert.assertNull(actualComment);
    }


}
